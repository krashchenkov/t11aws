# HowTo run NoteJam on AWS

target audience: engineers with basic knowledge of AWS, Linux console

## Prerequisites 
- terraform v11
- aim user with the next permission(ec2, s3)
- s3 bucket with name `tango-temp2` that belong to `eu-west-1` region (otherwise need to update  https://gitlab.com/krashchenkov/t11aws/-/blob/master/modules/backend.tf)
- git client

## Configure terraform
- dowload terraform version 11 (based on your OS and CPU architecture) https://releases.hashicorp.com/terraform/0.11.14/
- Terraform is a single binary that you should move to /usr/bin and make it executable.

    example:

        $ sudo mv terraform /usr/bin && sudo chmod +x

- if you have another OS please refer to https://dome.readthedocs.io/en/latest/dmc/deployment/install-terra.html
- check the version of terraform to be sure that terraform works for you

    example:
        Terraform v0.11.14

        Your version of Terraform is out of date! The latest version
        is 0.12.21. You can update by downloading from https://www.terraform.io/downloads.html

## Configure AWS access
- We need to configure access to AWS from the local machine. More details https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

    example:

        $ aws configure
        AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE
        AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY
        Default region name [None]: eu-west-1
        Default output format [None]: json

## Deploy infra & NoteJam application
- checkout terraform code to configure an environment

    example:
        
        $ git clone https://gitlab.com/krashchenkov/t11aws.git

- navigate to repo folder `t11aws` and run `terraform init` to initialize Terraform

    example:

        $ cd t11aws 
        $ terraform init ./modules
        Initializing the backend...

        Successfully configured the backend "s3"! Terraform will automatically
        use this backend unless the backend configuration changes.

        Initializing provider plugins...
        - Checking for available provider plugins on https://releases.hashicorp.com...
        - Downloading plugin for provider "aws" (2.50.0)...
        - Downloading plugin for provider "template" (2.1.2)...
        ...
            ~SOME_OUTPUT_HERE~
        ...

        Terraform has been successfully initialized!

- run `terraform plan` - This command will not create any resource on your AWS cloud. It lets you know what Terraform will do.

    example: 

        $ terraform plan ./modules
        Refreshing Terraform state in-memory prior to plan...
        The refreshed state will be used to calculate this plan, but will not be
        persisted to local or remote state storage.

        data.template_file.user_data: Refreshing state...
        data.aws_vpc.app_vpc: Refreshing state...
        data.aws_ami.ubuntu: Refreshing state...
        data.aws_subnet_ids.notejam_subnet_ids: Refreshing state...

        ------------------------------------------------------------------------

        An execution plan has been generated and is shown below.
        Resource actions are indicated with the following symbols:
        + create

        Terraform will perform the following actions:

        + aws_autoscaling_group.asg_notejam
            id:                                               <computed>
            arn:                                              <computed>
            default_cooldown:                                 <computed>
            desired_capacity:                                 <computed>
            enabled_metrics.#:                                "8"                       
            ...
                ~SOME_OUTPUT_HERE~
            ...

        Plan: 14 to add, 0 to change, 0 to destroy.

    Note that the (+) sign indicates that a resource will be created. In the other hand, when showing a minus sign (-), Terraform means that a resource will be deleted.

- In order to execute create an environment, need to excute `terraform apply -auto-approve`

    exmaple:

        $ terraform apply -auto-approve ./modules
        data.template_file.user_data: Refreshing state...
        data.aws_vpc.app_vpc: Refreshing state...
        data.aws_ami.ubuntu: Refreshing state...
        data.aws_subnet_ids.notejam_subnet_ids: Refreshing state...
        aws_security_group.notejam_server: Creating...
        arn:                    "" => "<computed>"
        description:            "" => "notejam Server: http"
        egress.#:               "" => "<computed>"
        ingress.#:              "" => "<computed>"
        name:                   "" => "it-sg-t1-notejam-server"
        owner_id:               "" => "<computed>"
        revoke_rules_on_delete: "" => "false"
        ...
            ~SOME_OUTPUT_HERE~
        ...
        Apply complete! Resources: 14 added, 0 changed, 0 destroyed.

        Outputs:

        lb-notejam = alb-t1-notejam-1925638866.eu-west-1.elb.amazonaws.com

    `Apply complete! Resources: 14 added, 0 changed, 0 destroyed.` - means env was deployed and ready to use. Beside that `Outputs` informs us about loadbalancer DNS name and we should use it to reach _NoteJam_ app on port _5000_. Based on Outputs above go to `http://alb-t1-notejam-1925638866.eu-west-1.elb.amazonaws.com:5000`

## Destory env & application

- when you accomplished your work and env is not needed at all you can run `terraform destroy -auto-aprove ./modules` and env will be fully destroyed and app will be not reachable anymore

    NOTE: be 100% sure that env is not in use and can be destroyed. 
