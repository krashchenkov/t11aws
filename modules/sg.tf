resource "aws_security_group" "notejam_server" {
  name        = "${format("it-sg-%s-notejam-server", var.env)}"
  description = "notejam Server: http"
  vpc_id      = "${data.aws_vpc.app_vpc.id}"
  tags = {
    Name        = "${format("sg-%s-notejam-server", var.env)}"
    Environment = "${var.env}"
  }
}

resource "aws_security_group" "notejam_lb" {
  name        = "${format("it-sg-%s-notejam-lb", var.env)}"
  description = "notejam Server: http"
  vpc_id      = "${data.aws_vpc.app_vpc.id}"
  tags = {
    Name        = "${format("sg-%s-notejam-lb", var.env)}"
    Environment = "${var.env}"
  }
}

resource "aws_security_group_rule" "ingress_notejam_server_notejam_lb" {
  description       = "ingress_notejam_server_notejam_lb"
  type              = "ingress"
  from_port         = 80
  to_port           = 5000
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.notejam_lb.id}"
}

resource "aws_security_group_rule" "egress_notejam_lb" {
  description              = "egress_notejam_lb"
  type                     = "egress"
  from_port                = 5000
  to_port                  = 5000
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.notejam_server.id}"
  security_group_id        = "${aws_security_group.notejam_lb.id}"
}

resource "aws_security_group_rule" "notejam_ingress_notejam_lb" {
  description              = "notejam-notejam-lb"
  type                     = "ingress"
  from_port                = 5000
  to_port                  = 5000
  protocol                 = "tcp"
  source_security_group_id = "${aws_security_group.notejam_lb.id}"
  security_group_id        = "${aws_security_group.notejam_server.id}"
}

resource "aws_security_group_rule" "ingress_notejam_server_ssh" {
  description       = "ingress_notejam_server_notejam_ssh"
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.notejam_server.id}"
}

resource "aws_security_group_rule" "egress_notejam_server_notejam" {
  description       = "egress_notejam_server_notejam"
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.notejam_server.id}"
}
