#!/bin/bash
sudo apt-get update
sudo apt-get install git python-pip -y
cd /home/
git clone https://github.com/mimin0/notejam.git

cd notejam/flask/
sudo pip install -r requirements.txt

python runserver.py