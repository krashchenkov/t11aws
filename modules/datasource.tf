data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

data "template_file" "user_data" {
  template = "${file("${path.module}/user-data.sh")}"
}

data "aws_vpc" "app_vpc" {
  tags = {
    "Environment" = "${var.env}"
  }
}

data "aws_subnet_ids" "notejam_subnet_ids" {
  vpc_id = "${data.aws_vpc.app_vpc.id}"

  tags = {
    "Environment" = "${var.env}"
  }
}
