resource "aws_lb" "alb_notejam" {
  name               = "${format("alb-%s-notejam", var.env)}"
  internal           = false
  load_balancer_type = "application"
  idle_timeout       = 300
  security_groups    = ["${aws_security_group.notejam_lb.id}"]
  subnets            = ["${data.aws_subnet_ids.notejam_subnet_ids.ids}"]

  tags = {
    Name        = "${format("alb-%s-notejam", var.env)}"
    Environment = "${var.env}"
  }
}

resource "aws_lb_listener" "notejam_http" {
  load_balancer_arn = "${aws_lb.alb_notejam.arn}"
  port              = "5000"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.notejam_tg.arn}"
  }
}

resource "aws_lb_target_group" "notejam_tg" {
  name     = "${format("tg-%s-notejam-priv", var.env)}"
  port     = "5000"
  protocol = "HTTP"
  vpc_id   = "${data.aws_vpc.app_vpc.id}"

  tags = {
    Name        = "${format("tg-%s-notejam", var.env)}"
    Environment = "${var.env}"
  }

  health_check {
    interval            = 5
    healthy_threshold   = 3
    unhealthy_threshold = 2
    timeout             = 3
    protocol            = "HTTP"
    path                = "/signin"
    matcher             = "200,301"
  }
}
