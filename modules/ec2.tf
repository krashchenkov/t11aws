resource "aws_launch_template" "lt_notejam" {
  name_prefix   = "${format("lt-%s-notejam-", var.env)}"
  image_id      = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  key_name      = "notejam"
  user_data     = "${base64encode(data.template_file.user_data.rendered)}"

  iam_instance_profile {
    name = "${aws_iam_instance_profile.notejam_profile.name}"
  }

  network_interfaces {
    associate_public_ip_address = true
    delete_on_termination       = true
    security_groups             = ["${aws_security_group.notejam_server.id}"]
  }

  credit_specification {
    cpu_credits = "standard"
  }

  block_device_mappings {
    device_name = "/dev/xvda"

    ebs {
      volume_type = "gp2"
      volume_size = "20"
    }
  }

  monitoring {
    enabled = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg_notejam" {
  name                      = "${format("asg-%s-notejam", var.env)}"
  vpc_zone_identifier       = ["${data.aws_subnet_ids.notejam_subnet_ids.ids}"]
  target_group_arns         = ["${aws_lb_target_group.notejam_tg.arn}"]
  health_check_grace_period = 300
  health_check_type         = "ELB"
  force_delete              = false
  termination_policies      = ["OldestInstance"]
  min_size                  = "1"
  max_size                  = "1"
  min_elb_capacity          = "1"

  launch_template = {
    id      = "${aws_launch_template.lt_notejam.id}"
    version = "$$Latest"
  }

  lifecycle {
    create_before_destroy = true
  }

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupPendingInstances",
    "GroupStandbyInstances",
    "GroupTerminatingInstances",
    "GroupTotalInstances",
  ]

  metrics_granularity = "1Minute"

  tag {
    key                 = "Name"
    value               = "Notejam"
    propagate_at_launch = true
  }
}
